@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Question</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="{{route('pertanyaan.index')}}">Question</a></li>
          <li class="breadcrumb-item active">Show</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Detail Question</h3>
      </div>
      <!-- /.card-header -->

      <div class="card-body">
        <div class="tab-content">
          <div class="active tab-pane" id="activity">
            <!-- Post -->
            <div class="post">
              {{csrf_field()}}
              <div class="user-block">
                <img class="img-circle img-bordered-sm" src="/dist/img/user2-160x160.jpg" alt="user image">
                <span class="username">
                  <a href="#">{{$data_pertanyaan->judul}}</a>
                  <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                </span>
                <span class="description">Created at - {{$data_pertanyaan->created_at}} | Updated at - {{$data_pertanyaan->updated_at}}</span>
              </div>
              <!-- /.user-block -->
              <p>
                {{$data_pertanyaan->isi}}
              </p>

              <p>
                <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                <span class="float-right">
                  <a href="#" class="link-black text-sm">
                    <i class="far fa-comments mr-1"></i> Comments (5)
                  </a>
                </span>
              </p>

              <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
            </div>
            <!-- /.post -->
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
</section>
<!-- /.content -->
@endsection