@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Question</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="/pertanyaan/index">Question</a></li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit Question</h3>
      </div>
      <!-- /.card-header -->

      <div class="card-body">
        <form action="{{ route('pertanyaan.update', $data_pertanyaan->id) }}" method="POST">
          {{csrf_field()}}
          @method('put')
          <div class="row">
            <div class="col-4">
              <label for="judul">Title</label>
              <input name="judul" type="text" class="form-control bg-light" id="judul" value="{{$data_pertanyaan->judul}}" required>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <label for="isi">Content</label>
              <textarea name="isi" class="form-control bg-light" id="isi" rows="5" required>{{$data_pertanyaan->isi}}</textarea>
            </div>
          </div>
          <hr>
          <button type="submit" class="btn btn-success btn-sm "><i class="fas fa-save"></i> SAVE</button>
          <a class="btn btn-danger btn-sm" href="{{route('pertanyaan.index')}}" role="button"><i class="fas fa-undo"></i>
            CANCEL</a>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</section>
<!-- /.content -->
@endsection