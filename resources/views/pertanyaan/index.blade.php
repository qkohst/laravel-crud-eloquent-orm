@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Question</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="/pertanyaan/index">Question</a></li>
          <li class="breadcrumb-item active">Index</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    @if(session('success'))
    <div class="callout callout-success alert alert-success alert-dismissible fade show" role="alert">
      <h5><i class="fas fa-check"></i> Success :</h5>
      {{session('success')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Table Question</h3>
      </div>
      <div class="card-body">
        <button type="button" class="btn btn-sm my-1 btn-primary" data-toggle="modal" data-target="#createPertanyaan"><i class="fas fa-plus"></i>
          Post a question
        </button>
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No.</th>
              <th>Title</th>
              <th>Contents</th>
              <th>Created at</th>
              <th>Updated at</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 0; ?>
            @foreach($data_pertanyaan as $pertanyaan)
            <?php $no++; ?>
            <tr>
              <td>{{$no}}</td>
              <td>{{$pertanyaan->judul}}</td>
              <td>{{$pertanyaan->isi}}</td>
              <td>{{$pertanyaan->created_at}}</td>
              <td>{{$pertanyaan->updated_at}}</td>
              <td>
                <form action="{{ route('pertanyaan.destroy', $pertanyaan->id) }}" method="post">
                  @csrf
                  @method('delete')
                  <a href="{{route('pertanyaan.show',$pertanyaan->id)}}" class=" btn btn-success btn-sm my-1 mr-sm-1"><i class="nav-icon fas fa-eye"></i> Show</a>
                  <a href="{{route('pertanyaan.edit',$pertanyaan->id)}}" class="btn btn-warning btn-sm my-1 mr-sm-1"><i class="nav-icon fas fa-pencil-alt"></i> Edit</a>
                  <button type="submit" class="btn btn-danger btn-sm my-1 mr-sm-1" onclick="return confirm('Delete this question ?')"><i class="nav-icon fas fa-trash"></i>
                    Delete</button>
                </form>
              </td>
            </tr>

            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- Modal Tambah -->
    <div class="modal fade" id="createPertanyaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h5 class="modal-title" id="exampleModalLabel"><i class="nav-icon fas fa-question-circle my-1 btn-sm-1"></i> Post A Question</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="{{route('pertanyaan.store')}}" method="POST">
              {{csrf_field()}}
              <div class="row">
                <label for="title">Title</label>
                <input value="{{old('title')}}" name="title" type="text" class="form-control bg-light" id="title" placeholder="Title question" required>
                <label for="semester">Contents</label>
                <textarea name="content" class="form-control bg-light" id="content" rows="5" placeholder="Contents question" required>{{old('content')}}</textarea>
              </div>
              <hr>
              <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-save"></i>
                POST</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->
@endsection

@push('scripts')
<script src="/plugins/datatables/jquery.dataTables.js"></script>
<script src="/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function() {
    $("#example1").DataTable();
  });
</script>
@endpush