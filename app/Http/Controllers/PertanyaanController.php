<?php

namespace App\Http\Controllers;

use App\Pertanyaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', compact('data_pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pertanyaan = new Pertanyaan();
        $pertanyaan->judul   = $request->input('title');
        $pertanyaan->isi     = $request->input('content');
        $pertanyaan->save();
        return redirect()->route('pertanyaan.index')->with("success", "New Question Posted Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.show', compact('data_pertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.edit', compact('data_pertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pertanyaan = Pertanyaan::findorfail($id);

        $data_pertanyaan = [
            'judul' => $request->judul,
            'isi' => $request->isi,
        ];

        $pertanyaan->update($data_pertanyaan);

        return redirect()->route('pertanyaan.index')->with('success', 'Question Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_pertanyaan = Pertanyaan::find($id);
        $data_pertanyaan->delete();
        return redirect()->route('pertanyaan.index')->with('success', 'Question Deleted Successfully');
    }
}
